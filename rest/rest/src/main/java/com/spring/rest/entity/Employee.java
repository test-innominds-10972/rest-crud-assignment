package com.spring.rest.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Employee {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	
	private Long id;
	private String emp_id;
	private String fname;
	private String lname;
	private String email;
	private String phoneNumber;
	private String hiredate;
	private String job_id;
	private double sal;
	private String manager_id;
	
	

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getEmp_id() {
		return emp_id;
	}

	public void setEmp_id(String emp_id) {
		this.emp_id = emp_id;
	}

	public String getFname() {
		return fname;
	}

	public void setFname(String fname) {
		this.fname = fname;
	}

	public String getLname() {
		return lname;
	}

	public void setLname(String lname) {
		this.lname = lname;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this. email =  email;
	}
	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this. phoneNumber =  phoneNumber;
	}
	public String getHireDate() {
		return hiredate;
	}

	public void setHireDate(String hiredate) {
		this. hiredate =  hiredate;
	}
	public String getJob_id() {
		return job_id;
	}

	public void setJob_id(String job_id) {
		this. job_id =  job_id;
	}
	public double getSal() {
		return sal;
	}

	public void setSal(double sal) {
		this. sal =  sal;
	}
	public String getManager_id() {
		return manager_id;
	}

	public void setManager_id(String manager_id) {
		this. manager_id=  manager_id;
	}
	

	public Employee(Long id, String fname,String lname, String email,String phoneNumber, String hiredate,String job_id,double sal,String manager_id) {
		super();
		this.id = id;
		this.fname = fname;
		this.lname = lname;
		this.email =email;
		this.phoneNumber = phoneNumber;
		this.hiredate=hiredate;
		this.job_id=job_id;
		this.sal=sal;
		this.manager_id=manager_id;
	}

	public Employee() {
		super();
		// TODO Auto-generated constructor stub
	}

	
	
	
	

}