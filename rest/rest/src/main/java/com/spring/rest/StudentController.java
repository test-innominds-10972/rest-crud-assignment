package com.spring.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.spring.rest.entity.Student;
import com.spring.rest.entity.User;
import com.spring.rest.repository.StudentRepository;

@RestController
public class StudentController {

	@Autowired
	StudentRepository studentRepo;
	
	@GetMapping("/students")
	public List<Student> getAllStudents() {
		return studentRepo.findAll();
	}
	
	

	@GetMapping("/students/{id}")
	public Student getOnStudents(@PathVariable("id") Long id) {
		return studentRepo.getById(id);
		
		
	}
	
	@PostMapping("/students")
	public Student addStudents(@RequestBody Student requestStudents) {
		
		return studentRepo.save(requestStudents);
	}
	
	
	@PutMapping("/students/{id}")
	public Student updateStudents(@PathVariable("id") Long id, @RequestBody Student newStudentsValue)
	{
		
		Student s = studentRepo.getById(id);
		
		if(s!=null)
		{
			s.setAge(newStudentsValue.getAge());
			s.setName(newStudentsValue.getName());
			s.setGender(newStudentsValue.getGender());
			s.setClassi(newStudentsValue.getClassi());
		}
		return studentRepo.save(s);
	
	
}
	
	@DeleteMapping("/students/{id}")
	
	public String deleteStudents(@PathVariable("id") Long id)
	{
		@SuppressWarnings("deprecation")
		Student s = studentRepo.getById(id);
		
		if(s != null)
		{
			studentRepo.delete(s);
			return "student deleted";
		}
		else
		{
			return "student not available";
		}
	
		
		
}
	
	
}